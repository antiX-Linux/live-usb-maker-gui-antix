<?xml version="1.0" ?><!DOCTYPE TS><TS language="zh_CN" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Program_Name</source>
        <translation>程序名称</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="46"/>
        <source>Quit application</source>
        <translation>退出应用程序</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="49"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="56"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="88"/>
        <source>Display help </source>
        <translation>显示帮助</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="91"/>
        <source>Help</source>
        <translation>帮助(_H)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="114"/>
        <source>Back</source>
        <translation>后退</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <source>Next</source>
        <translation>下一页</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="198"/>
        <source>About this application</source>
        <translation>关于此软件</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <source>About...</source>
        <translation>关于…</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="208"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="239"/>
        <location filename="mainwindow.cpp" line="462"/>
        <location filename="mainwindow.cpp" line="481"/>
        <source>Select ISO</source>
        <translation>选择ISO文件</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select Target USB Device&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;选择目标 USB 设备&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="260"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select ISO file&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;选择 ISO 文件&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="289"/>
        <source>Advanced Options</source>
        <translation>高级选项</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="307"/>
        <source>Make the ext4 filesystem even if one exists</source>
        <translation>制作 ext4 文件系统，即使存在</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="314"/>
        <source>Save the original boot directory when updating a live-usb</source>
        <translation>更新 live-usb 时保存原来的 boot 目录</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="321"/>
        <source>Use gpt partitioning instead of msdos</source>
        <translation>使用 gpt 分区而不是 msdos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="324"/>
        <source>GPT partitioning</source>
        <translation>GPT 分区</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="331"/>
        <source>Update (only update an existing live-usb)</source>
        <translation>更新（只更新现有的 live-usb）</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <source>Don&apos;t replace syslinux files</source>
        <translation>不替换 syslinux 文件</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="341"/>
        <source>Keep syslinux files</source>
        <translation>保留 syslinux 文件</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="348"/>
        <source>Ignore USB/removable check</source>
        <translation>忽略 USB/可移动检查</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="355"/>
        <source>Temporarily disable automounting</source>
        <translation>暂时禁用自动挂载</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="365"/>
        <source>Set pmbr_boot disk flag (won&apos;t boot via UEFI)</source>
        <translation>设置 pmbr_boot 磁盘标志（不会通过 UEFI 启动）</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="372"/>
        <source>Don&apos;t use fuseiso to mount iso files</source>
        <translation>不使用 fuseiso 挂载 iso 文件</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="398"/>
        <source>Size of ESP (uefi) partition:</source>
        <translation>ESP (uefi) 分区的大小：</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="418"/>
        <source>Verbosity (less to more):</source>
        <translation>Verbosity（从少到多）：</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="457"/>
        <source>vfat</source>
        <translation>vfat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="462"/>
        <source>exfat</source>
        <translation>exfat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="467"/>
        <source>ext4</source>
        <translation>ext4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="472"/>
        <source>ntfs</source>
        <translation>ntfs</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="493"/>
        <source>Make separate data partition (percent)</source>
        <translation>制作单独的数据分区（百分比）</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="500"/>
        <source>Format</source>
        <translation>格式化</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="510"/>
        <source>Refresh drive list</source>
        <translation>刷新驱动器列表</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="524"/>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Show advanced options</source>
        <translation>显示高级选项</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="544"/>
        <source>Mode</source>
        <translation>模式</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="559"/>
        <source>Full-featured mode - writable LiveUSB</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="569"/>
        <source>Read-only, cannot be used with persistency</source>
        <translation>只读，不能与持久化一起使用</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="572"/>
        <source>Image mode - read-only LiveUSB (dd)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="586"/>
        <source>For distros other than antiX/MX use image mode (dd).</source>
        <translation>对于 antiX/MX 以外的发行版，请使用镜像模式 (dd)。</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="634"/>
        <source>Options</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="669"/>
        <source>Percent of USB-device to use:</source>
        <translation>使用的 USB 设备百分比：</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="676"/>
        <source>Label ext partition:</source>
        <translation>扩展分区标签：</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="724"/>
        <source>Don&apos;t run commands that affect the usb device</source>
        <translation>不运行会影响 USB 设备的命令</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="727"/>
        <source>Dry run (no change to system)</source>
        <translation>试运行（不改变系统）</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="734"/>
        <source>clone from a mounted live-usb or iso-file.</source>
        <translation>从挂载的 live-usb 或 iso 文件克隆。</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="737"/>
        <source>Clone an existing live system</source>
        <translation>克隆已有的 live 系统</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="744"/>
        <source>Set up to boot from an encrypted partition, will prompt for pass phrase on first boot</source>
        <translation>设置为从加密分区启动，将在首次启动时提示输入密码</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <source>Encrypt</source>
        <translation>加密</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="754"/>
        <source>Clone running live system</source>
        <translation>克隆运行中的 live 系统</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <location filename="mainwindow.cpp" line="267"/>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Failure</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <source>Source and destination are on the same device, please select again.</source>
        <translation>源和目标在同一台设备上，请重新选择。</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="111"/>
        <source>Writing %1 using 'dd' command to /dev/%2,

Please wait until the the process is completed</source>
        <translation>使用“dd”命令将 %1 写入 /dev/%2，

请等待过程完成</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>LiveUSB creation successful!</source>
        <translation>LiveUSB 创建成功！</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="267"/>
        <source>Error encountered in the LiveUSB creation process</source>
        <translation>LiveUSB创建过程中遇到的错误</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="316"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="316"/>
        <source>Please select a USB device to write to</source>
        <translation>请选择要写入的 USB 设备</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="319"/>
        <location filename="mainwindow.cpp" line="475"/>
        <source>clone</source>
        <translation>克隆</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="355"/>
        <source>About %1</source>
        <translation>关于 %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="356"/>
        <source>Version: </source>
        <translation>版本:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>Program for creating a live-usb from an iso-file, another live-usb, a live-cd/dvd, or a running live system.</source>
        <translation>用于从 iso 文件、另一个 live-usb、live-cd/dvd 或正在运行的 live 系统创建 live-usb 的程序。</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <source>Copyright (c) MX Linux</source>
        <translation>版权所有(c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="360"/>
        <source>%1 License</source>
        <translation>%1 许可证</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="368"/>
        <source>%1 Help</source>
        <translation>%1 帮助</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="379"/>
        <source>Select an ISO file to write to the USB drive</source>
        <translation>选择要写入 U 盘的 ISO 文件</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="387"/>
        <location filename="mainwindow.cpp" line="458"/>
        <source>Select Source Directory</source>
        <translation>选择源目录</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Could not find %1/antiX/linuxfs file</source>
        <translation>找不到 %1/antiX/linuxfs 文件</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Hide advanced options</source>
        <translation>隐藏高级选项</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="457"/>
        <location filename="mainwindow.cpp" line="473"/>
        <source>Select Source</source>
        <translation>选择源</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="461"/>
        <location filename="mainwindow.cpp" line="479"/>
        <source>Select ISO file</source>
        <translation>选择 ISO 文件</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>许可证</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>更新日志</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;关闭</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <source>You must run this program as root.</source>
        <translation>您必须以root用户身份运行该程序。</translation>
    </message>
</context>
</TS>