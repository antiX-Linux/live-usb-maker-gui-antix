<?xml version="1.0" ?><!DOCTYPE TS><TS language="es_ES" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Program_Name</source>
        <translation>Nombre_del_Programa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="46"/>
        <source>Quit application</source>
        <translation>Salir de la aplicación</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="49"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="56"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="88"/>
        <source>Display help </source>
        <translation>Mostrar la ayuda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="91"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="114"/>
        <source>Back</source>
        <translation>Atras</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <source>Next</source>
        <translation>Siguiente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="198"/>
        <source>About this application</source>
        <translation>Acerca de esta aplicación</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <source>About...</source>
        <translation>Acerca de...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="208"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="239"/>
        <location filename="mainwindow.cpp" line="462"/>
        <location filename="mainwindow.cpp" line="481"/>
        <source>Select ISO</source>
        <translation>Seleccione ISO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select Target USB Device&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Seleccione Dispositivo USB&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="260"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select ISO file&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Seleccione archivo ISO&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="289"/>
        <source>Advanced Options</source>
        <translation>Opciones avanzadas</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="307"/>
        <source>Make the ext4 filesystem even if one exists</source>
        <translation>Crear el sistema de archivos ext4 incluso si existe uno</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="314"/>
        <source>Save the original boot directory when updating a live-usb</source>
        <translation>Guardar el directorio de arranque original al actualizar un live-usb</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="321"/>
        <source>Use gpt partitioning instead of msdos</source>
        <translation>Use particionamiento gpt en lugar de msdos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="324"/>
        <source>GPT partitioning</source>
        <translation>Particionamiento GPT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="331"/>
        <source>Update (only update an existing live-usb)</source>
        <translation>Actualizar (solamente actualiza un liveUSB existente)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <source>Don&apos;t replace syslinux files</source>
        <translation>No reemplazar archivos syslinux</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="341"/>
        <source>Keep syslinux files</source>
        <translation>Mantener archivos syslinux</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="348"/>
        <source>Ignore USB/removable check</source>
        <translation>Ignorar comprobacion USB/extraible</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="355"/>
        <source>Temporarily disable automounting</source>
        <translation>Desactivar temporalmente el montaje automático</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="365"/>
        <source>Set pmbr_boot disk flag (won&apos;t boot via UEFI)</source>
        <translation>Establecer el indicador de disco de arranque pmbr (no arrancará a través de UEFI)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="372"/>
        <source>Don&apos;t use fuseiso to mount iso files</source>
        <translation>No use fuseiso para montar archivos iso</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="398"/>
        <source>Size of ESP (uefi) partition:</source>
        <translation>Tamaño de la partición ESP (uefi):</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="418"/>
        <source>Verbosity (less to more):</source>
        <translation>Verbosidad (de menos a más):</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="457"/>
        <source>vfat</source>
        <translation>vfat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="462"/>
        <source>exfat</source>
        <translation>exfat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="467"/>
        <source>ext4</source>
        <translation>ext4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="472"/>
        <source>ntfs</source>
        <translation>ntfs</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="493"/>
        <source>Make separate data partition (percent)</source>
        <translation>Crear una partición de datos separada (porcentaje)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="500"/>
        <source>Format</source>
        <translation>Formato</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="510"/>
        <source>Refresh drive list</source>
        <translation>Refrescar lista de dispositivos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="524"/>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Show advanced options</source>
        <translation>Mostrar opciones avanzadas</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="544"/>
        <source>Mode</source>
        <translation>Modo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="559"/>
        <source>Full-featured mode - writable LiveUSB</source>
        <translation>Modo con todas las funciones: LiveUSB grabable</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="569"/>
        <source>Read-only, cannot be used with persistency</source>
        <translation>Solo lectura, no se puede usar con persistencia</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="572"/>
        <source>Image mode - read-only LiveUSB (dd)</source>
        <translation>Modo de imagen: LiveUSB (dd) de solo lectura</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="586"/>
        <source>For distros other than antiX/MX use image mode (dd).</source>
        <translation>Para distribuciones que no sean antiX/MX use el modo de imagen (dd).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="634"/>
        <source>Options</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="669"/>
        <source>Percent of USB-device to use:</source>
        <translation>Porcentaje del dispositivo USB a usar:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="676"/>
        <source>Label ext partition:</source>
        <translation>Etiqueta de partición ext:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="724"/>
        <source>Don&apos;t run commands that affect the usb device</source>
        <translation>No ejecute comandos que afecten al dispositivo USB</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="727"/>
        <source>Dry run (no change to system)</source>
        <translation>Modo simulación (sin cambios en el sistema)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="734"/>
        <source>clone from a mounted live-usb or iso-file.</source>
        <translation>clonar desde un archivo iso o live-usb montado.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="737"/>
        <source>Clone an existing live system</source>
        <translation>Clonar un sistema en vivo existente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="744"/>
        <source>Set up to boot from an encrypted partition, will prompt for pass phrase on first boot</source>
        <translation>Configurado para arrancar desde una partición cifrada, solicitará una frase de contraseña en el primer arranque</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <source>Encrypt</source>
        <translation>Cifrar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="754"/>
        <source>Clone running live system</source>
        <translation>Clonar sistema en vivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <location filename="mainwindow.cpp" line="267"/>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Failure</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <source>Source and destination are on the same device, please select again.</source>
        <translation>El origen y el destino están en el mismo dispositivo, seleccione nuevamente.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="111"/>
        <source>Writing %1 using 'dd' command to /dev/%2,

Please wait until the the process is completed</source>
        <translation>Escribiendo %1 con el comando &apos;dd&apos; hacia /dev/%2,

Por favor espere hasta que se complete el proceso</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>Success</source>
        <translation>Exito</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>LiveUSB creation successful!</source>
        <translation>¡Grabacion del Live-USB exitosa!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="267"/>
        <source>Error encountered in the LiveUSB creation process</source>
        <translation>Error encontrado en el proceso de grabacion del Live-USB</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="316"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="316"/>
        <source>Please select a USB device to write to</source>
        <translation>Por favor seleccione dispositivo USB a grabar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="319"/>
        <location filename="mainwindow.cpp" line="475"/>
        <source>clone</source>
        <translation>clonar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="355"/>
        <source>About %1</source>
        <translation>Acerca de %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="356"/>
        <source>Version: </source>
        <translation>Versión: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>Program for creating a live-usb from an iso-file, another live-usb, a live-cd/dvd, or a running live system.</source>
        <translation>Programa para grabar un live-usb a partir de un archivo iso, otro live-usb, un live-cd/dvd o un sistema en vivo en ejecución.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="360"/>
        <source>%1 License</source>
        <translation>%1 Licencia</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="368"/>
        <source>%1 Help</source>
        <translation>%1 Ayuda</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="379"/>
        <source>Select an ISO file to write to the USB drive</source>
        <translation>Seleccione un archivo ISO para grabar en la unidad USB</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="387"/>
        <location filename="mainwindow.cpp" line="458"/>
        <source>Select Source Directory</source>
        <translation>Seleccionar directorio de origen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Could not find %1/antiX/linuxfs file</source>
        <translation>No se pudo encontrar el archivo /antiX/linuxfs %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Hide advanced options</source>
        <translation>Ocultar opciones avanzadas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="457"/>
        <location filename="mainwindow.cpp" line="473"/>
        <source>Select Source</source>
        <translation>Seleccione origen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="461"/>
        <location filename="mainwindow.cpp" line="479"/>
        <source>Select ISO file</source>
        <translation>Seleccionar archivo ISO</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Registro de cambios</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <source>You must run this program as root.</source>
        <translation>Debe ejecutar este programa como root.</translation>
    </message>
</context>
</TS>