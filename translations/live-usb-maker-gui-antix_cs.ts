<?xml version="1.0" ?><!DOCTYPE TS><TS language="cs" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Program_Name</source>
        <translation>Program_Name</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="46"/>
        <source>Quit application</source>
        <translation>Ukončit aplikaci</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="49"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="56"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="88"/>
        <source>Display help </source>
        <translation>Zobrazit nápovědu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="91"/>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="114"/>
        <source>Back</source>
        <translation>Předchozí</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <source>Next</source>
        <translation>Další</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="198"/>
        <source>About this application</source>
        <translation>O této aplikaci</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <source>About...</source>
        <translation>O programu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="208"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="239"/>
        <location filename="mainwindow.cpp" line="462"/>
        <location filename="mainwindow.cpp" line="481"/>
        <source>Select ISO</source>
        <translation>Vybrat ISO soubor</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select Target USB Device&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Vyberte cílové USB zařízení&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="260"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select ISO file&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Vyberte ISO soubor&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="289"/>
        <source>Advanced Options</source>
        <translation>Rozšířené možnosti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="307"/>
        <source>Make the ext4 filesystem even if one exists</source>
        <translation>Vytvořit ext4 souborový systém i když už existuje</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="314"/>
        <source>Save the original boot directory when updating a live-usb</source>
        <translation>Při aktuallizaci live-usb ponechat původní bootovací složku </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="321"/>
        <source>Use gpt partitioning instead of msdos</source>
        <translation>použít gpt rozdělení místo msdos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="324"/>
        <source>GPT partitioning</source>
        <translation>Rozdělení GPT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="331"/>
        <source>Update (only update an existing live-usb)</source>
        <translation>Update (pouze aktualizace live-usb)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <source>Don&apos;t replace syslinux files</source>
        <translation>Nepřepisovat soubory syslinux</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="341"/>
        <source>Keep syslinux files</source>
        <translation>Ponechat soubory syslinux</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="348"/>
        <source>Ignore USB/removable check</source>
        <translation>Ignorovat kontrolu USB</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="355"/>
        <source>Temporarily disable automounting</source>
        <translation>Dočasně vypnout autopřipojení</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="365"/>
        <source>Set pmbr_boot disk flag (won&apos;t boot via UEFI)</source>
        <translation>Nastavit pmbr_boot disk flag (nenabootuje pod UEFI)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="372"/>
        <source>Don&apos;t use fuseiso to mount iso files</source>
        <translation>Nepoužívat fuseiso k připojení iso souborů</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="398"/>
        <source>Size of ESP (uefi) partition:</source>
        <translation>Velikost ESP (uefi) oddílu:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="418"/>
        <source>Verbosity (less to more):</source>
        <translation>Verbosita (méně nebo více):</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="457"/>
        <source>vfat</source>
        <translation>vfat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="462"/>
        <source>exfat</source>
        <translation>exfat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="467"/>
        <source>ext4</source>
        <translation>ext4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="472"/>
        <source>ntfs</source>
        <translation>ntfs</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="493"/>
        <source>Make separate data partition (percent)</source>
        <translation>Vytvořit separátní oddíl pro data (percent)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="500"/>
        <source>Format</source>
        <translation>Formát</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="510"/>
        <source>Refresh drive list</source>
        <translation>Obnovit seznam diskových jednotek</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="524"/>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Show advanced options</source>
        <translation>Zobrazit pokročilé možnosti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="544"/>
        <source>Mode</source>
        <translation>Mód</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="559"/>
        <source>Full-featured mode - writable LiveUSB</source>
        <translation>Ze všemi funkcemi - zapisovatelné LiveUSB</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="569"/>
        <source>Read-only, cannot be used with persistency</source>
        <translation>Read-only, nemůže být použito s persitencí</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="572"/>
        <source>Image mode - read-only LiveUSB (dd)</source>
        <translation>Mód Image - LiveUSB pouze pro čtení (dd)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="586"/>
        <source>For distros other than antiX/MX use image mode (dd).</source>
        <translation>Pro ostatní distra, ne antiX/MX použijte Mód Image (dd).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="634"/>
        <source>Options</source>
        <translation>Možnosti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="669"/>
        <source>Percent of USB-device to use:</source>
        <translation>Procent USB disku k použití:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="676"/>
        <source>Label ext partition:</source>
        <translation>Jméno ext oddílu:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="724"/>
        <source>Don&apos;t run commands that affect the usb device</source>
        <translation>Nespouštět přikazy které ovlivňují usb disk</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="727"/>
        <source>Dry run (no change to system)</source>
        <translation>Zkouška (beze změn v systému)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="734"/>
        <source>clone from a mounted live-usb or iso-file.</source>
        <translation>Klonování připojeného live-usb disku nebo iso-souboru.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="737"/>
        <source>Clone an existing live system</source>
        <translation>Naklonovat již existující live systém</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="744"/>
        <source>Set up to boot from an encrypted partition, will prompt for pass phrase on first boot</source>
        <translation>Nastaví bootování ze šifrovaného oddílu, při prvním startu bude vyžadováno heslo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <source>Encrypt</source>
        <translation>Zašifrovat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="754"/>
        <source>Clone running live system</source>
        <translation>Naklonovat běžící Live systém</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <location filename="mainwindow.cpp" line="267"/>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Failure</source>
        <translation>Selhání</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <source>Source and destination are on the same device, please select again.</source>
        <translation>Zdroj a cíl jsou na stejné jednotce, prosím vyberte znovu.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="111"/>
        <source>Writing %1 using 'dd' command to /dev/%2,

Please wait until the the process is completed</source>
        <translation>Zápis %1 s použitím příkazu &apos;dd&apos; na /dev/%2,

Čekejte až bude proces dokončen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>Success</source>
        <translation>Úspěch!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>LiveUSB creation successful!</source>
        <translation>Vytvoření Live-USB proběhlo úspěšně!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="267"/>
        <source>Error encountered in the LiveUSB creation process</source>
        <translation>Při procesu tvorby Live-USB došlo k chybě</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="316"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="316"/>
        <source>Please select a USB device to write to</source>
        <translation>Prosím vyberte USB jednotku k zápisu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="319"/>
        <location filename="mainwindow.cpp" line="475"/>
        <source>clone</source>
        <translation>klonovat</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="355"/>
        <source>About %1</source>
        <translation>O %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="356"/>
        <source>Version: </source>
        <translation>Verze:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>Program for creating a live-usb from an iso-file, another live-usb, a live-cd/dvd, or a running live system.</source>
        <translation>Program pro tvorbu Live-USB z ISO, jiného Live-USB, Live-CD/DVD, nebo spuštěného live systému.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Vlastnická práva (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="360"/>
        <source>%1 License</source>
        <translation>Licence %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="368"/>
        <source>%1 Help</source>
        <translation>Nápověda %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="379"/>
        <source>Select an ISO file to write to the USB drive</source>
        <translation>Vyberte ISO soubor pro zápis na USB jednotku </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="387"/>
        <location filename="mainwindow.cpp" line="458"/>
        <source>Select Source Directory</source>
        <translation>Vyberte zdrojový aresář</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Could not find %1/antiX/linuxfs file</source>
        <translation> %1/antiX/linuxfs soubor nenalezen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Hide advanced options</source>
        <translation>Skrýt pokročilé možnosti</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="457"/>
        <location filename="mainwindow.cpp" line="473"/>
        <source>Select Source</source>
        <translation>Vyberte zdroj</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="461"/>
        <location filename="mainwindow.cpp" line="479"/>
        <source>Select ISO file</source>
        <translation>Vyberte ISO soubor</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Protokol změn</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Zavřít</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <source>You must run this program as root.</source>
        <translation>Tuto aplikaci musíte spustit jako root.</translation>
    </message>
</context>
</TS>