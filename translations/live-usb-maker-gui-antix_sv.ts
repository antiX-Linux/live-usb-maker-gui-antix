<?xml version="1.0" ?><!DOCTYPE TS><TS language="sv" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Program_Name</source>
        <translation>Program_Namn</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="46"/>
        <source>Quit application</source>
        <translation>Avsluta programmet</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="49"/>
        <source>Close</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="56"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="88"/>
        <source>Display help </source>
        <translation>Visa hjälp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="91"/>
        <source>Help</source>
        <translation>Hjälp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="114"/>
        <source>Back</source>
        <translation>Tillbaka</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <source>Next</source>
        <translation>Nästa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="198"/>
        <source>About this application</source>
        <translation>Om detta program</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <source>About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="208"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="239"/>
        <location filename="mainwindow.cpp" line="462"/>
        <location filename="mainwindow.cpp" line="481"/>
        <source>Select ISO</source>
        <translation>Välj ISO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select Target USB Device&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Välj Mål USB-Enhet&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="260"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select ISO file&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Välj ISO-fil&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="289"/>
        <source>Advanced Options</source>
        <translation>Avancerade Alternativ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="307"/>
        <source>Make the ext4 filesystem even if one exists</source>
        <translation>Skapa ext4 filsystem även om ett finns</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="314"/>
        <source>Save the original boot directory when updating a live-usb</source>
        <translation>Spara den ursprungliga boot-katalogen när en live-usb uppdateras</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="321"/>
        <source>Use gpt partitioning instead of msdos</source>
        <translation>Använd gpt partionering i stället för msdos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="324"/>
        <source>GPT partitioning</source>
        <translation>GPT partionering</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="331"/>
        <source>Update (only update an existing live-usb)</source>
        <translation>Uppdatera (uppdatera endast en existerande live-usb)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <source>Don&apos;t replace syslinux files</source>
        <translation>Ersätt inte syslinux filer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="341"/>
        <source>Keep syslinux files</source>
        <translation>Behåll syslinux filer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="348"/>
        <source>Ignore USB/removable check</source>
        <translation>Gör inte USB/borttagbar kontroll</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="355"/>
        <source>Temporarily disable automounting</source>
        <translation>Inaktivera automontering tillfälligt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="365"/>
        <source>Set pmbr_boot disk flag (won&apos;t boot via UEFI)</source>
        <translation>Välj pmbr_boot disk flagga (bootar inte via UEFI)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="372"/>
        <source>Don&apos;t use fuseiso to mount iso files</source>
        <translation>Använd inte fuseiso för att montera iso-filer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="398"/>
        <source>Size of ESP (uefi) partition:</source>
        <translation>Storlek på ESP (uefi) partition:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="418"/>
        <source>Verbosity (less to more):</source>
        <translation>Ordrikedom (mindre till mer)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="457"/>
        <source>vfat</source>
        <translation>vfat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="462"/>
        <source>exfat</source>
        <translation>exfat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="467"/>
        <source>ext4</source>
        <translation>ext4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="472"/>
        <source>ntfs</source>
        <translation>ntfs</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="493"/>
        <source>Make separate data partition (percent)</source>
        <translation>Gör en separat data-partition (procent)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="500"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="510"/>
        <source>Refresh drive list</source>
        <translation>Förnya disk-listan</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="524"/>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Show advanced options</source>
        <translation>Visa avancerade alternativ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="544"/>
        <source>Mode</source>
        <translation>Läge</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="559"/>
        <source>Full-featured mode - writable LiveUSB</source>
        <translation>Komplett läge - skrivbar LiveUSB</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="569"/>
        <source>Read-only, cannot be used with persistency</source>
        <translation>Read-only,  kan inte användas med persistens</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="572"/>
        <source>Image mode - read-only LiveUSB (dd)</source>
        <translation>Avbildningsläge - skrivskyddad LiveUSB (dd)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="586"/>
        <source>For distros other than antiX/MX use image mode (dd).</source>
        <translation>För andra distributioner än antiX/MX, använd avbildningsläge (dd).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="634"/>
        <source>Options</source>
        <translation>Valmöjligheter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="669"/>
        <source>Percent of USB-device to use:</source>
        <translation>Procent av USB-enhet som ska användas:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="676"/>
        <source>Label ext partition:</source>
        <translation>Etikettera ext partition:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="724"/>
        <source>Don&apos;t run commands that affect the usb device</source>
        <translation>Kör inte kommandon som påverkar usb-enheten</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="727"/>
        <source>Dry run (no change to system)</source>
        <translation>Testkör (ingen förändring av systemet)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="734"/>
        <source>clone from a mounted live-usb or iso-file.</source>
        <translation>klona från en monterad  live-usb eller iso-fil</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="737"/>
        <source>Clone an existing live system</source>
        <translation>Klona ett existerande live-system</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="744"/>
        <source>Set up to boot from an encrypted partition, will prompt for pass phrase on first boot</source>
        <translation>Arrangera för att boota från en krypterad partition, kommer att fråga efter ett lösenord vid första start</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <source>Encrypt</source>
        <translation>Kryptera</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="754"/>
        <source>Clone running live system</source>
        <translation>Klona det live-system som körs</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <location filename="mainwindow.cpp" line="267"/>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Failure</source>
        <translation>Misslyckande</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <source>Source and destination are on the same device, please select again.</source>
        <translation>Källa och mål är på samma enhet, var vänlig välj igen.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="111"/>
        <source>Writing %1 using 'dd' command to /dev/%2,

Please wait until the the process is completed</source>
        <translation>Skriver %1 med&apos;dd&apos; kommandot till /dev/%2,

Var vänlig vänta tills processen är färdig</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>Success</source>
        <translation>Det lyckades</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>LiveUSB creation successful!</source>
        <translation>Skapandet av LiveUSB gick bra</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="267"/>
        <source>Error encountered in the LiveUSB creation process</source>
        <translation>Fel uppträdde i processen att skapa LiveUSB</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="316"/>
        <source>Error</source>
        <translation>Fel</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="316"/>
        <source>Please select a USB device to write to</source>
        <translation>Var vänlig välj en USB enhet att skriva till</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="319"/>
        <location filename="mainwindow.cpp" line="475"/>
        <source>clone</source>
        <translation>klona</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="355"/>
        <source>About %1</source>
        <translation>Om %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="356"/>
        <source>Version: </source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>Program for creating a live-usb from an iso-file, another live-usb, a live-cd/dvd, or a running live system.</source>
        <translation>Program för att skapa en live-usb från en iso-fil, en annan live-usb, en live-cd/dvd, eller ett live-system som körs.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="360"/>
        <source>%1 License</source>
        <translation>%1 Licens</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="368"/>
        <source>%1 Help</source>
        <translation>%1 Hjälp</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="379"/>
        <source>Select an ISO file to write to the USB drive</source>
        <translation>Välj en ISO-fil att skriva till USB-enheten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="387"/>
        <location filename="mainwindow.cpp" line="458"/>
        <source>Select Source Directory</source>
        <translation>Välj Käll Katalog</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Could not find %1/antiX/linuxfs file</source>
        <translation>Kunde inte hitta %1/antiX/linuxfs filen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Hide advanced options</source>
        <translation>Dölj avancerade val</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="457"/>
        <location filename="mainwindow.cpp" line="473"/>
        <source>Select Source</source>
        <translation>Välj Källa</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="461"/>
        <location filename="mainwindow.cpp" line="479"/>
        <source>Select ISO file</source>
        <translation>Välj ISO-fil</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Ändringslogg</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Close</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <source>You must run this program as root.</source>
        <translation>Du måste köra detta program som root</translation>
    </message>
</context>
</TS>