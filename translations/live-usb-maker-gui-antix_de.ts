<?xml version="1.0" ?><!DOCTYPE TS><TS language="de" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Program_Name</source>
        <translation>Program_Name</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="46"/>
        <source>Quit application</source>
        <translation>Anwendung beenden</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="49"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="56"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="88"/>
        <source>Display help </source>
        <translation>Hilfe anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="91"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="114"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <source>Next</source>
        <translation>Weiter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="198"/>
        <source>About this application</source>
        <translation>Impressum zu diesem Programm</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <source>About...</source>
        <translation>Impressum</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="208"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="239"/>
        <location filename="mainwindow.cpp" line="462"/>
        <location filename="mainwindow.cpp" line="481"/>
        <source>Select ISO</source>
        <translation>ISO-Datei auswählen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select Target USB Device&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Ziel-USB-Gerät auswählen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="260"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select ISO file&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ISO-Datei auswählen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="289"/>
        <source>Advanced Options</source>
        <translation>Zusatzoptionen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="307"/>
        <source>Make the ext4 filesystem even if one exists</source>
        <translation>Erzeugung des ext4 Dateisystems, auch falls es bereits eines gibt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="314"/>
        <source>Save the original boot directory when updating a live-usb</source>
        <translation>Beim Aktualisieren eines Live-USB-Systems original Boot-Verzeichnis speichern</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="321"/>
        <source>Use gpt partitioning instead of msdos</source>
        <translation>GPT- statt MSDOS-Partitionstabelle benutzen </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="324"/>
        <source>GPT partitioning</source>
        <translation>GPT-Partitionstabelle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="331"/>
        <source>Update (only update an existing live-usb)</source>
        <translation>Aktualisierung (nur eines bereits existenten Livs-USB-Systems)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <source>Don&apos;t replace syslinux files</source>
        <translation>syslinux-Dateien nicht ersetzen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="341"/>
        <source>Keep syslinux files</source>
        <translation>syslinux-Dateien beibehalten</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="348"/>
        <source>Ignore USB/removable check</source>
        <translation>Prüfung auf USB/Wechsellaufwerk ignorieren</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="355"/>
        <source>Temporarily disable automounting</source>
        <translation>Automount temporär abschalten</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="365"/>
        <source>Set pmbr_boot disk flag (won&apos;t boot via UEFI)</source>
        <translation>Festplattenmarkierung pmbr_boot setzen (verhindert das Booten mit UEFI)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="372"/>
        <source>Don&apos;t use fuseiso to mount iso files</source>
        <translation>zum Einbinden von .iso Datei nicht fuseiso benutzen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="398"/>
        <source>Size of ESP (uefi) partition:</source>
        <translation>Größe der ESP (uefi) Partition:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="418"/>
        <source>Verbosity (less to more):</source>
        <translation>Ausführlichkeitslevel der angezeigten Infos (niedrig bis hoch)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="457"/>
        <source>vfat</source>
        <translation>vfat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="462"/>
        <source>exfat</source>
        <translation>exfat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="467"/>
        <source>ext4</source>
        <translation>ext4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="472"/>
        <source>ntfs</source>
        <translation>ntfs</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="493"/>
        <source>Make separate data partition (percent)</source>
        <translation>Eigenständige Partition für Daten erzeugen (in Prozent)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="500"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="510"/>
        <source>Refresh drive list</source>
        <translation>Laufwerksliste erneuern</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="524"/>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Show advanced options</source>
        <translation>Zusatzoptionen anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="544"/>
        <source>Mode</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="559"/>
        <source>Full-featured mode - writable LiveUSB</source>
        <translation>Voll funktioneller Modus - beschreibbares LiveUSB</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="569"/>
        <source>Read-only, cannot be used with persistency</source>
        <translation>Nur lesbar: die Dauerhaftigkeitsfunktion kann nicht verwendet werden</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="572"/>
        <source>Image mode - read-only LiveUSB (dd)</source>
        <translation>Abbild-Modus - nur lesbares LiveUSB (dd)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="586"/>
        <source>For distros other than antiX/MX use image mode (dd).</source>
        <translation>Für andere Distributionen als antiX/MX verwende den Abbild-Modus (dd).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="634"/>
        <source>Options</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="669"/>
        <source>Percent of USB-device to use:</source>
        <translation>Nutzbarer Prozentanteil des USB-Geräts</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="676"/>
        <source>Label ext partition:</source>
        <translation>Bezeichnung der ext Partition:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="724"/>
        <source>Don&apos;t run commands that affect the usb device</source>
        <translation>Keine Befehle ausführen, die das USB-Gerät betreffen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="727"/>
        <source>Dry run (no change to system)</source>
        <translation>Probelauf (ohne Änderungen des Systems)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="734"/>
        <source>clone from a mounted live-usb or iso-file.</source>
        <translation>Klonen eines eingehängten Live-USB- oder ISO-Datei-Systems</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="737"/>
        <source>Clone an existing live system</source>
        <translation>Duplizierung eines existierenden Live-Systems</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="744"/>
        <source>Set up to boot from an encrypted partition, will prompt for pass phrase on first boot</source>
        <translation>Einstellung auf ein Booten von einer verschlüsselten Partition; beim ersten Bootvorgang wird ein Passwort verlangt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <source>Encrypt</source>
        <translation>Verschlüsselung</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="754"/>
        <source>Clone running live system</source>
        <translation>Klonen eines laufenden Live-Systems</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <location filename="mainwindow.cpp" line="267"/>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Failure</source>
        <translation>Fehlschlag</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <source>Source and destination are on the same device, please select again.</source>
        <translation>Quelle und Ziel befinden sich auf demselben Gerät; bitte Auswahl wiederholen.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="111"/>
        <source>Writing %1 using 'dd' command to /dev/%2,

Please wait until the the process is completed</source>
        <translation>%1 wird mittels &apos;dd&apos; Befehl auf /dev/%2 geschrieben,

biite auf Abschluß des Vorgangs warten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>Success</source>
        <translation>Erfolg</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>LiveUSB creation successful!</source>
        <translation>LiveUSB-Erstellung erfolgreich!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="267"/>
        <source>Error encountered in the LiveUSB creation process</source>
        <translation>Bei der LiveUSB-Erzeugung trat ein Fehler auf</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="316"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="316"/>
        <source>Please select a USB device to write to</source>
        <translation>Ziel-USB-Gerät auswählen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="319"/>
        <location filename="mainwindow.cpp" line="475"/>
        <source>clone</source>
        <translation>Klonen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="355"/>
        <source>About %1</source>
        <translation>Über %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="356"/>
        <source>Version: </source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>Program for creating a live-usb from an iso-file, another live-usb, a live-cd/dvd, or a running live system.</source>
        <translation>Programm zur LiveUSB-Erzeugung aus einer ISO-Datei, einem anderen LiveUSB-System, einer Live-CD/DVD oder einem laufenden Live-System.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="360"/>
        <source>%1 License</source>
        <translation>%1 Lizenz</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="368"/>
        <source>%1 Help</source>
        <translation>%1 Hilfe</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="379"/>
        <source>Select an ISO file to write to the USB drive</source>
        <translation>Auswahl der auf das USB-Laufwerk zu schreibenden ISO-Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="387"/>
        <location filename="mainwindow.cpp" line="458"/>
        <source>Select Source Directory</source>
        <translation>Auswahl Quellverzeichnis</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Could not find %1/antiX/linuxfs file</source>
        <translation>%1/antiX/linuxfs Datei konnte nicht gefunden werden</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Hide advanced options</source>
        <translation>Erweiterte Optionen ausblenden</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="457"/>
        <location filename="mainwindow.cpp" line="473"/>
        <source>Select Source</source>
        <translation>Quelle auswählen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="461"/>
        <location filename="mainwindow.cpp" line="479"/>
        <source>Select ISO file</source>
        <translation>ISO-Datei auswählen</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Changelog</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Close</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <source>You must run this program as root.</source>
        <translation>Dieses Programm muss vom Benutzer &quot;root&quot; ausgeführt werden.</translation>
    </message>
</context>
</TS>