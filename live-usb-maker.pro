# **********************************************************************
# * Copyright (C) 2017 MX Authors
# *
# * Authors: Adrian
# *          MX Linux <http://mxlinux.org>
# *
# * This is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with this package. If not, see <http://www.gnu.org/licenses/>.
# **********************************************************************/

QT       += widgets
CONFIG   += release warn_on c++1z

TARGET = live-usb-maker-gui-antix
TEMPLATE = app

# The following define makes your compiler warn you if you use any
# feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp\
    mainwindow.cpp \
    cmd.cpp \
    about.cpp

HEADERS  += \
    mainwindow.h \
    version.h \
    cmd.h \
    about.h

FORMS    += \
    mainwindow.ui

TRANSLATIONS += translations/live-usb-maker-gui-antix_ca.ts \
                translations/live-usb-maker-gui-antix_cs.ts \
                translations/live-usb-maker-gui-antix_da.ts \
                translations/live-usb-maker-gui-antix_de.ts \
                translations/live-usb-maker-gui-antix_el.ts \
		translations/live-usb-maker-gui-antix_es_ES.ts \
                translations/live-usb-maker-gui-antix_es.ts \
                translations/live-usb-maker-gui-antix_fi.ts \
		translations/live-usb-maker-gui-antix_fr_BE.ts \
                translations/live-usb-maker-gui-antix_fr.ts \
                translations/live-usb-maker-gui-antix_gl_ES.ts \
		translations/live-usb-maker-gui-antix_hi.ts \
                translations/live-usb-maker-gui-antix_hu.ts \
                translations/live-usb-maker-gui-antix_it.ts \
                translations/live-usb-maker-gui-antix_ja.ts \
                translations/live-usb-maker-gui-antix_nb.ts \
                translations/live-usb-maker-gui-antix_nl.ts \
                translations/live-usb-maker-gui-antix_pl.ts \
                translations/live-usb-maker-gui-antix_pt.ts \
                translations/live-usb-maker-gui-antix_pt_BR.ts \
                translations/live-usb-maker-gui-antix_ru.ts \
                translations/live-usb-maker-gui-antix_sl.ts \
                translations/live-usb-maker-gui-antix_sq.ts \
                translations/live-usb-maker-gui-antix_sv.ts \
                translations/live-usb-maker-gui-antix_tr.ts \  
                translations/live-usb-maker-gui-antix_zh_CN.ts

RESOURCES += \
    images.qrc
